<?php

require_once "model/UserModel.php";
require_once "AuthView.php";
require_once 'UserController.php';

$model = new UserModel();
$view = new AuthView();

$username = empty($_POST['username']) ? '' : strtolower(trim($_POST['username']));
$password = empty($_POST['password']) ? '' : trim($_POST['password']);

$user = '';
$contentPage = 'form-login';


if (!empty($_SESSION['userInfo'])) {
    $contentPage = 'success';
    $user = $_SESSION['userInfo'];
}
if (!empty($username) && !empty($password)) {
    $user = $model -> getUserByNamePass($username, $password);
    if (is_array($user)) {
        $contentPage = "success";
        $_SESSION['userInfo'] = $user;
    }
}


//$model->create("joe",1234, "joe", "awesome"); // change pars to what you want to pass


$view -> show('header');
$view -> show($contentPage, $user);
$view -> show('footer');
