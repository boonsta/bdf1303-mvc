<?php
// Include the database because it was needed
require_once "config/db.php";

class UserModel {

    public function getUserByNamePass($name, $pass) {
        //The constructer method wasn't working, but this works
        $db = new PDO(MY_DSN, MY_USER, MY_PASS);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $db -> prepare("
	        SELECT user_id AS id, user_name AS name, user_fullname AS fullname, user_email AS email
	        FROM users
	        WHERE (user_name = :name)
	        AND (user_password = MD5(CONCAT(user_salt, :pass)))
        ");

        if ($stmt -> execute(array(':name' => $name, ':pass' => $pass))) {
            $rows = $stmt -> fetchAll(\PDO::FETCH_ASSOC);
            if (count($rows) === 1) {
                return $rows[0];
            }
        }
        //i dont understand?it is telling you that it does not find a column nmae oo its user_password
        //how come it didnt show up? it did look again o ok!ok so now I? you need to put things together
        // you are running everything from auth controller
        // YOu need a html page with links
        //link to add user form
        //link to login form
        // once the user logs in show all the products (read data)
        // I put pretty much everything you need there. Oh ok! Thats where that was. I was wondering what you were
        //talking about in your email? Thank you!np Thank you for walking me through this, It helped ALOT! yougot it, try to get these to work and let me know.
        //ok I will.
        return FALSE;
    }

    //run your code for me
    //ok for your application, what needs to happen?
    //Set up a form to sign up that CREATES a user
    //ok ome thing i see on your code, i think you can remove

    public function create($un, $pass, $fname, $email) {// keep going
        global $db;
        $sql = "INSERT INTO users
                (user_name, user_password, user_fullname, user_email)
                values
                (:un, :pass, :fname, :email)";

        //end then
        $db = new PDO(MY_DSN, MY_USER, MY_PASS);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $db -> prepare($sql);
        //ok, that makes sense
        //this is where it is different?yes insert does not return anything so you would only need to execute
        //so just execute my statement?yes
        $stmt -> execute(array(':un' => $un, ':pass' => $pass, ':fname' => $fname, ':email' => $email));
        // you are missing parms. here
        //but dont I need to return false?no does not return anything false is something o ok
        //what does an => mean? array key and value connector in PHP ok
        // this should insert the parameters into the db now go to the controller who is calling this model

    }

    public function update($un, $pass, $fname, $email) {
        global $db;
        $sql = "UPDATE SET users
                (user_name, user_password, user_fullname, user_email)
                values
                (:un, :pass, :fname, :email)
                WHERE user_id = :id";

        $db = new PDO(MY_DSN, MY_USER, MY_PASS);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $db -> prepare($sql);
        $stmt -> execute(array(':un' => $un, ':pass' => $pass, ':fname' => $fname, ':email' => $email));

    }

    public function delete($id) {
        global $db;
        $sql = "DELETE FROM users
                WHERE user_id = :id";
        $stmt = $db -> prepare($sql);
        $stmt -> execute(array(":id" => $id));
    }

}
