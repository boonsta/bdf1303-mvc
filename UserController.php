<?php

require_once "model/UserModel.php";

$model = new UserModel();
$view = new AuthView();


$username = empty($_POST['username']) ? '' : strtolower(trim($_POST['username']));
$password = empty($_POST['password']) ? '' : trim($_POST['password']);
$fullname = empty($_POST['fullname']) ? '' : strtolower(trim($_POST['fullname']));
$email = empty($_POST['email']) ? '' : trim($_POST['email']);

$user = '';
$signup = 'form-signup'; 
session_start();

if (!empty($username) && !empty($password) && !empty($fullname) && !empty($email)) {
    $user = $model -> create($username, $password, $fullname, $email);
    if (is_array($user)) {
        return TRUE;
       } 
    }


//$model->create($username,$password,$fullname,$email);


$view -> show('header');
$view->show('form-signup');
$view -> show('footer');
