<?php

class AuthView {

	public function show($template, $data = array()) {
		$templatePath = "view/${template}.inc";
		if (file_exists($templatePath)) {
			include $templatePath;
		}
	}

}
